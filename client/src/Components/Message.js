import React from 'react'

const Message = ({item}) => {
    //console.log(item);
    return (
        <div>
            <h2>{item.text}</h2>
            <h3>{item.user}</h3>
        </div>
    )
}

export default Message
