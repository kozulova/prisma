import React, {useState} from 'react'
import { MESSAGE_QUERY, POST_MESSAGE_MUTATION, POST_LIKE_MUTATION} from '../queries';

const MessageForm = () => {
    const [message, setMessage] = useState('');
    const postMessage = () => {
        const message = document.querySelector('#message').value;
        console.log(message);
        setMessage('');
    }
    return (
        <div>
            <input type="text" id="message" value={message} onChange={(e)=>setMessage(e.target.value)}/>
            <button onClick={postMessage}>Submit</button>
        </div>
    )
}

export default MessageForm
