import React from 'react'
import { Query } from 'react-apollo'
import { MESSAGE_QUERY, POST_MESSAGE_MUTATION, POST_LIKE_MUTATION} from '../queries';
import Message from './Message';

const MessagesList = () => {
    return (
        <div>
            MessageList
            <Query query={MESSAGE_QUERY}>
                {({loading, error, data, subscribeToMore}) => {
                    if(loading) return <div>Loading ...</div>;
                    if(error) return <div>Fetch error</div>
                    const {messages: {messageList}} = data;

                    return (
                        <div className="message-list">
                            {messageList.map(item => {
                                return <Message item={item} key={item.id}/>
                            })}
                        </div>
                    )
                }}
            </Query>
        </div>
    )
}

export default MessagesList
