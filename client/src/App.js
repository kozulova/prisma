import './App.css';
import MessagesList from './Components/MessagesList';
import {Link, Route, Switch} from 'react-router-dom';
import MessageForm from './Components/MessageForm';

function App() {
  return (
    <div className="App">
    <div className="nav-panel">
      <Link to="/">Messages</Link>
      <Link to="/add-message">Add message</Link>
    </div>
    <Switch>
    <Route excat path="/add-message" component={MessageForm}/>
    <Route excat path="/" component={MessagesList}/>
    </Switch>  
    </div>
  );
}

export default App;
