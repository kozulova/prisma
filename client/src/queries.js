import gql from 'graphql-tag';

export const MESSAGE_QUERY = gql`
    query{
        messages{
            messageList{
                id
                text
                user
                likes{
                    like
                    dislike
                }
            }
        }
    }
`;

export const POST_MESSAGE_MUTATION = gql`
    mutation PostMutation($text: String!, $user: String!){
    postMessage(text: $text, user: $user){
        id
        text
        user
        likes{
            like
            dislike
        }
    }
    }
`;

export const POST_LIKE_MUTATION = gql`
    mutation PostMutation($messagetId: ID!, $like: Boolean!, $dislike: Boolean!){
    postLike(messageId: $messageId, like: $like, dislike: $dislike){
        id
        like,
        dislike
    }
    }
`;