const {GraphQLServer} = require('graphql-yoga');
const Query = require('./resolvers/Query');
const {prisma} = require('./prisma/generated/prisma-client/index');
const Mutation = require('./resolvers/Mutation');
const Subscription = require('./resolvers/Subscription');
const Message = require('./resolvers/Message');
const Like = require('./resolvers/Like');

const resolvers = {
    Query,
    Mutation,
    Subscription,
    Message,
    Like
};

const server = new GraphQLServer({
    typeDefs: './src/schema.graphql',
    resolvers,
    context: {prisma},
    debug: true
})

console.log(process.env);

server.start(()=> console.log('4000 Port'));