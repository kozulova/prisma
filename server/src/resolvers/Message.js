function likes (parent, args, context) {
    return context.prisma.message({
        id: parent.id
    }).likes();
}

module.exports = {
    likes
}