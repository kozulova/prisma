function postMessage (parent, args, context, info){
    return context.prisma.createMessage({
        text: args.text,
        user: args.user
    });
}

async function postLike(parent, args, context, info){
    const messageExists = await context.prisma.$exists.message({
        id: args.messageId
    });

    if(!messageExists){
        throw new Error(`Message with ID ${args.messageId} doesn't exist`);
    }

    return context.prisma.createLike({
        text: args.text,
        message: {connect: {id: args.messageId}}
    })
}

module.exports = {
    postMessage,
    postLike
}